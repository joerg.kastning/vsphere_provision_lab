vsphere_provision_lab
=====================

This Ansible role helps to provision predefined lab environments of virtual machines on VMware vSphere Clusters.
It can be used to define lab envirioments in YAML dictionaries to provision and re-provision your lab envirionments.
VMware vSphere Templates are used to clone and customize VMs.

Requirements
------------

The role uses the Ansible modules `vmware_guest` and `vmware_guest_disk` which are part of the [community.vmware collection](https://galaxy.ansible.com/community/vmware):

 * [community.vmware.vmware_guest module – Manages virtual machines in vCenter](https://docs.ansible.com/ansible/latest/collections/community/vmware/vmware_guest_module.html)
 * [community.vmware.vmware_guest_disk module – Manage disks related to virtual machine in given vCenter infrastructure](https://docs.ansible.com/ansible/latest/collections/community/vmware/vmware_guest_disk_module.html)

Below requirements are needed on the host that executes the modules:

 * python >= 2.6
 * PyVmomi

VMware vSphere Templates needs to exist that can be used with this role to clone and customize.

How to use this role?
---------------------

 1. Register IP addresses and FQDNs in your DHCP/DNS management system for the VMs you are going to create. This step may be optional depending on your environment.
 2. Set values for all variables mentined in the 'Role Variables' section. See the [Ansible User Guide/Using Variables](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html) for general information on how to use variables.
 3. Include the role to your playbook. (See example below in section 'Example Playbook')
 4. Execute the playbook.

Role Variables
--------------

The following variables are defined by this role and needs to be set to values appropriate for your environment. Example values for all variables can be found in `defaults/main.yml`.

### Variables for vCenter

The following variables define the credentials and the vCenter Server to connect to. It's recommended to store these information in some protected vault file that's not readable to everyone.

 * `vsphere_provision_lab_vc_hostname` - The FQDN for your VMware vCenter Server Appliance.
 * `vsphere_provision_lab_vc_username` - A user name with the permissions to clone/create new VMs.
 * `vsphere_provision_lab_vc_password` - The users password.
 * `vsphere_provision_lab_vc_datacenter` - The name of the vSphere Datacenter you would like to use.
 * `vsphere_provision_lab_vc_cluster` - The vSphere Cluster name where the VMs should be provisioned.
 * `vsphere_provision_lab_vc_datastore` - Name of the datastore where the VMDK file should be created.
 * `vsphere_provision_lab_vc_folder` - The vSphere folder name where the VM is created.

### VM Dictionary

VM names and parameters are specified using the YAML dictionary `vsphere_provision_lab_guests`. The dictionary keys are used as the VM name. The following code block shows an example where a VM called 'vm1' is going to be created with these parameters:

 * Created from template 'rhel8-en'.
 * With 1 vCPU and 512 MB of Memory.
 * vNIC placed in vm_network called VLAN123 with ip 192.168.0.1/24 and gateway at 192.198.0.254.
 * 'host1' is set as hostname for the guest OS.
 * IP addresses for DNS servers and domain suffixes are configured.
 * A second HDD is created for this VM.

~~~
  vm1:
    vm_template: rhel8-en
    vm_ram_mb: 1024
    vm_vcpu: 2
    vm_net: VLAN123
    vm_ip: 192.168.0.5
    vm_netmask: 255.255.255.0
    vm_gateway: 192.168.0.254
    vm_domain: sub.example.com
    vm_hostname: host2
    vm_dns_servers:
      - 192.168.0.2
      - 192.168.0.3
    vm_dns_suffix:
      - sub.example.com
      - sub2.example.com
      - sub3.example.com
    vm_second_hdd: true
    vm_second_hdd_size_gb: "10"
    vm_second_hdd_type: "thin"
    vm_second_hdd_datastore: "DS1"
    vm_scsi_controller: "1"
    vm_unit_number: "1"
    vm_scsi_type: 'paravirtual'
~~~

In case you don't need a second HDD, you set `vm_second_hdd: false`. The other variables regarding the second HDD could be left empty in this case.

Dependencies
------------

No other roles are required to use this role.

Example Playbook
----------------

~~~
---
- hosts: localhost
  connection: local
  gather_facts: no
  vars_files:
    - /path/to/vault-with-vcenter-creds.vars
    - roles/vsphere_provision_lab/vars/rhel-lab.yml
 
  roles:
    - vsphere_provision_lab
~~~

The examples a playbook using two vars files:

 * **/path/to/vault-with-vcenter-creds.vars** - In this file all vCenter related variables are stored. It's recommended to encrypt this file using ansible-vault.
 * **roles/vsphere_provision_lab/vars/rhel-lab.yml** - Relative path to a file containing a dictionary that defines the hosts/VMs for the lab that should be created.

License
-------

GPL-2.0-or-later

Author Information
------------------

Jörg Kastning <joerg (dot) kastning (@) uni-bielefeld (dot) de>
